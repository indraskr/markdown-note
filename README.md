# Mark Note

A simple note taking app that uses markdown.

<hr>

## Setup / Installation

```bash
npm i # for backend dependencies
```

```bash
npm i --prefix frontend # for frontend dependencies
```

## Run locally

Rename `.env.example > .env` and write your configs

then simply run

```bash
npm run dev
# or
yarn dev
```

`@TODO add guide for deploying`
